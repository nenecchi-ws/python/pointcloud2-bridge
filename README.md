<!----------------------------------------------------------------------------------------------------------------------
#
#   Title
#
# --------------------------------------------------------------------------------------------------------------------->
# PointCloud2 Bridge

<!----------------------------------------------------------------------------------------------------------------------
#
#   Badge
#
# --------------------------------------------------------------------------------------------------------------------->
[![pipeline status](https://gitlab.com/nenecchi-ws/ros/library/pointcloud2-bridge/badges/main/pipeline.svg)](https://gitlab.com/nenecchi-ws/ros/library/pointcloud2-bridge/-/commits/main)
[![coverage report](https://gitlab.com/nenecchi-ws/ros/library/pointcloud2-bridge/badges/main/coverage.svg)](https://gitlab.com/nenecchi-ws/ros/library/pointcloud2-bridge/-/commits/main)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Doc style: Google](https://img.shields.io/badge/%20style-google-3666d6.svg)](https://google.github.io/styleguide/pyguide.html)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

<!----------------------------------------------------------------------------------------------------------------------
#
#   Description
#
# --------------------------------------------------------------------------------------------------------------------->
This is a ROS Library of bidirectional conversion between sensor_msgs/PointCloud2 and Numpy.

![Input Image](docs/image.png)

<!----------------------------------------------------------------------------------------------------------------------
#
#   Table of Contents
#
# --------------------------------------------------------------------------------------------------------------------->
## Table of Contents
  * [Requirement](#requirement)
  * [Getting Started](#getting-started)
    * [Installation](#installation)
  * [Usage](#usage)
  * [Example](#example)
  * [Contributing](#contributing)
  * [License](#license)
<!----------------------------------------------------------------------------------------------------------------------
#
#   Requirement
#
# --------------------------------------------------------------------------------------------------------------------->
## Requirement
* Required
  * Ubuntu 20.04
  * ROS Noetic
  * Python 3.8

<!----------------------------------------------------------------------------------------------------------------------
#
#   Getting Started
#
# --------------------------------------------------------------------------------------------------------------------->
## Getting Started

### Installation
This project has not been released on PyPI.  

#### Pip
Install from the remote repository.
```shell
pip install git+https://gitlab.com/nenecchi-ws/ros/library/pointcloud2-bridge.git
```

#### Clone
Clone to your project by `git clone` or `git submodule` and make a path in some way (e.g., Symbolic link).
```shell
git clone https://gitlab.com/nenecchi-ws/ros/library/pointcloud2-bridge.git
```
```shell
git submodule add https://gitlab.com/nenecchi-ws/ros/library/pointcloud2-bridge.git
```

<!----------------------------------------------------------------------------------------------------------------------
#
#   Usage
#
# --------------------------------------------------------------------------------------------------------------------->
## Usage

Convert `sensor_msgs/PointCloud2` to `dict[str, numpy.ndarray]`
```python
import sensor_msgs.msg as sensor_msgs
from pointcloud2_bridge import PointCloud2Bridge

pc2_bridge = PointCloud2Bridge()

def callback(msg: sensor_msgs.PointCloud2):
    np_dict = pc2_bridge.pointcloud2_to_numpy(msg)
```

Converts `numpy.ndarray` to `sensor_msgs/PointCloud2`
```python
import numpy as np
import std_msgs.msg as std_msgs
from pointcloud2_bridge import PointCloud2Bridge

pc2_bridge = PointCloud2Bridge()

xyz = np.random.rand(480, 640, 3).astype(np.float32)
rgb = np.random.randint(0, 256, (480, 640, 3), np.uint8)
header = std_msgs.Header()

msg = pc2_bridge.numpy_to_pointcloud2(xyz, rgb=rgb, header=header)
```

<!----------------------------------------------------------------------------------------------------------------------
#
#   Example
#
# --------------------------------------------------------------------------------------------------------------------->
## Example
The example codes based on the Jupyter Notebook are provided.
* [example1.ipynb](example/example1.ipynb)
* [example2.ipynb](example/example2.ipynb)

<!----------------------------------------------------------------------------------------------------------------------
#
#   Contributing
#
# --------------------------------------------------------------------------------------------------------------------->
## Contributing
See [CONTRIBUTING](CONTRIBUTING.md) to learn about CI.

<!----------------------------------------------------------------------------------------------------------------------
#
#   License
#
# --------------------------------------------------------------------------------------------------------------------->
## License
MIT License (see [LICENSE](LICENSE)).
