from __future__ import annotations

import numpy as np
import numpy.typing as npt
import sensor_msgs.msg as sensor_msgs
import std_msgs.msg as std_msgs

from ._modules import T, raw_numpy_to_pointcloud2, raw_pointcloud2_to_numpy_dict


class PointCloud2Bridge:
    """
    Bidirectional conversion between sensor_msgs/PointCloud2 and Numpy.

    References:
        http://wiki.ros.org/pcl/Overview

    """

    _REFORMAT_KEYS = {"x", "y", "z", "rgb", "rgba"}

    # ------------------------------------------------------------------------------------------------------------------
    #
    #   Public Method
    #
    # ------------------------------------------------------------------------------------------------------------------
    @staticmethod
    def pointcloud2_to_numpy(msg: sensor_msgs.PointCloud2, bgr_to_rgb: bool = False) -> dict[str, npt.NDArray[T]]:
        """
        Converts sensor_msgs/PointCloud2 to Numpy.

        Returns:
            {"xyz": [H, W, 3], "rgb": [H, W, 3], "rgba": [H, W, 4], ...}.

        """
        ndarray_dict = raw_pointcloud2_to_numpy_dict(msg)
        ndarray_dict["xyz"] = np.stack([ndarray_dict.pop(k) for k in ["x", "y", "z"]], axis=-1).astype(np.float32)

        if "rgb" in ndarray_dict:
            rgb_uint32 = ndarray_dict["rgb"]
            if bgr_to_rgb:
                shift = np.array([16, 8, 0], np.uint8)
            else:
                shift = np.array([0, 8, 16], np.uint8)
            rgb = ((rgb_uint32[..., None].view(np.uint32) >> shift) & 255).astype(np.uint8)
            ndarray_dict["rgb"] = rgb

        if "rgba" in ndarray_dict:
            rgba_uint32 = ndarray_dict["rgba"]
            if bgr_to_rgb:
                shift = np.array([16, 8, 0, 24], np.uint8)
            else:
                shift = np.array([0, 8, 16, 24], np.uint8)
            ndarray_dict["rgba"] = ((rgba_uint32[..., None].view(np.uint32) >> shift) & 255).astype(np.uint8)

        return ndarray_dict

    @classmethod
    def numpy_to_pointcloud2(
        cls,
        xyz: npt.NDArray[np.float32],
        header: std_msgs.Header,
        *,
        rgb: (npt.NDArray[np.uint8] | None) = None,
        rgba: (npt.NDArray[np.uint8] | None) = None,
        others: (dict[str, npt.NDArray[T]] | None) = None,
        force_dense: bool = False,
        rgb_to_bgr: bool = False,
    ):
        """
        Converts Numpy to sensor_msgs/PointCloud2.

        Args:
            xyz: [H, W, 3], np.float32
            header: ROS message
            rgb: [H, W, 3], np.uint8
            rgba: [H, W, 3], np.uint8
            others: [H, W]
            force_dense: Remove nan, inf, and -inf or Not
            rgb_to_bgr: Replace RGB to BGR

        """
        if (rgb is not None) and (rgba is not None):
            raise ValueError("\"rgb\" and \"rgba\" cannot set simultaneously")

        attributes = {}

        if rgb is not None:
            rgb_uint32 = rgb.astype(np.uint32)
            if rgb_to_bgr:
                attributes["rgb"] = (rgb_uint32[..., 0] << 16) | (rgb_uint32[..., 1] << 8) | (rgb_uint32[..., 2] << 0)
            else:
                attributes["rgb"] = (rgb_uint32[..., 0] << 0) | (rgb_uint32[..., 1] << 8) | (rgb_uint32[..., 2] << 16)

        if rgba is not None:
            rgba_uint32 = rgba.astype(np.uint32)
            if rgb_to_bgr:
                attributes["rgba"] = (
                    (rgba_uint32[..., 0] << 16)
                    | (rgba_uint32[..., 1] << 8)
                    | (rgba_uint32[..., 2] << 0)
                    | (rgba_uint32[..., 3] << 24)
                )
            else:
                attributes["rgba"] = (
                    (rgba_uint32[..., 0] << 0)
                    | (rgba_uint32[..., 1] << 8)
                    | (rgba_uint32[..., 2] << 16)
                    | (rgba_uint32[..., 3] << 24)
                )

        if others is not None:
            attributes.update(others)

        return raw_numpy_to_pointcloud2(
            xyz[..., 0],
            xyz[..., 1],
            xyz[..., 2],
            attributes,
            header,
            force_dense,
        )
