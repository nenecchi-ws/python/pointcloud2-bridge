from __future__ import annotations

from typing import Type

import numpy as np
import numpy.typing as npt
import sensor_msgs.msg as sensor_msgs
import std_msgs.msg as std_msgs

from ._pointfield_bridge import PointFieldBridge, T

__all__ = ["raw_pointcloud2_to_numpy_dict", "raw_numpy_to_pointcloud2"]


# ----------------------------------------------------------------------------------------------------------------------
#
#   Public Method
#
# ----------------------------------------------------------------------------------------------------------------------
def raw_pointcloud2_to_numpy_dict(msg: sensor_msgs.PointCloud2) -> dict[str, npt.NDArray[T]]:
    dtype, field_names = PointFieldBridge.point_fields_to_dtypes(msg.fields, msg.point_step)
    array = np.frombuffer(msg.data, dtype).reshape((msg.height, msg.width))
    return {name: array[name] for name in field_names}


def raw_numpy_to_pointcloud2(
    x: npt.NDArray[(np.float32 | np.float64)],
    y: npt.NDArray[(np.float32 | np.float64)],
    z: npt.NDArray[(np.float32 | np.float64)],
    others: dict[str, npt.NDArray[T]],
    header: std_msgs.Header,
    force_dense: bool,
) -> sensor_msgs.PointCloud2:
    """
    ...

    Args:
        x: [N] or [H, W]
        y: [N] or [H, W]
        z: [N] or [H, W]
        others: [N] or [H, W] or [H, W, N]
        header: ...
        force_dense: ...

    """
    for key, value in others.items():
        if (value.ndim == x.ndim) and (value.shape != x.shape):
            raise ValueError(f"\"{key}\" in others must be a shape of {x.shape}, but got {value.shape}")
        if (value.ndim == x.ndim + 1) and (value.shape[:-1] != x.shape):
            raise ValueError(
                f"\"{key}\" in others must be a shape of {[*x.shape[:-1], value.shape[-1]]}, but got {value.shape}"
            )

    dtype_dict: dict[str, tuple[Type[T], int]] = {"x": (np.float32, 1), "y": (np.float32, 1), "z": (np.float32, 1)}
    dtype_list = [(name, value[0]) for name, value in dtype_dict.items()]
    for key, value in others.items():
        if value.ndim == x.ndim + 1:
            t = (value.dtype.type, value.shape[-1])
            d = np.dtype((value.dtype.type, value.shape[-1]))
        else:
            t = (value.dtype.type, 1)
            d = value.dtype.type
        dtype_dict[key] = t  # pyright: ignore [reportGeneralTypeIssues]
        dtype_list.append((key, d))  # pyright: ignore [reportGeneralTypeIssues]

    H, W = (1, x.shape[0]) if x.ndim == 1 else x.shape
    array = np.empty(shape=(H, W), dtype=np.dtype(dtype_list))
    array["x"] = x
    array["y"] = y
    array["z"] = z

    for key, value in others.items():
        array[key] = value

    return _numpy_to_ros_msgs(array, dtype_dict, header, force_dense)


# ----------------------------------------------------------------------------------------------------------------------
#
#   Private Methods
#
# ----------------------------------------------------------------------------------------------------------------------
def _numpy_to_ros_msgs(
    array: npt.NDArray[np.void],
    dtype_dict: dict[str, tuple[Type[T], int]],
    header: std_msgs.Header,
    force_dense: bool,
) -> sensor_msgs.PointCloud2:
    """
    ...

    Args:
        array: [H, W]
        dtype_dict: ...
        header: ...
        force_dense: ...

    """
    if force_dense:
        stack = np.stack(
            [np.isfinite(array[name]) for name in array.dtype.names], axis=0  # pyright: ignore [reportOptionalIterable]
        )
        y = np.all(stack, axis=0)
        if not np.all(y).item():
            array = array[y][None, ...]

    fields, point_step = PointFieldBridge.dtype_to_fields(array.dtype, dtype_dict)
    height, width = array.shape

    return sensor_msgs.PointCloud2(
        header=header,
        height=height,
        width=width,
        fields=fields,
        is_bigendian=False,
        point_step=point_step,
        row_step=point_step * width,
        data=array.tobytes(),
        is_dense=force_dense,
    )
