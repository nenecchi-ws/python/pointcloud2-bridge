from __future__ import annotations

from typing import Final, Type, Union

import numpy as np
import sensor_msgs.msg as sensor_msgs
from sensor_msgs.msg import PointField

__all__ = ["T", "PointFieldBridge"]

T = Union[np.int8, np.uint8, np.int16, np.uint16, np.int32, np.uint32, np.float32, np.float64]


class PointFieldBridge:

    _point_fields_to_np_dtypes: Final[dict[int, Type[T]]] = {
        PointField.INT8: np.int8,
        PointField.UINT8: np.uint8,
        PointField.INT16: np.int16,
        PointField.UINT16: np.uint16,
        PointField.INT32: np.int32,
        PointField.UINT32: np.uint32,
        PointField.FLOAT32: np.float32,
        PointField.FLOAT64: np.float64,
    }

    _np_dtypes_to_point_fields: Final[dict[Type[T], int]] = {v: k for k, v in _point_fields_to_np_dtypes.items()}

    _point_field_to_size: Final[dict[int, int]] = {
        PointField.INT8: 1,
        PointField.UINT8: 1,
        PointField.INT16: 2,
        PointField.UINT16: 2,
        PointField.INT32: 4,
        PointField.UINT32: 4,
        PointField.FLOAT32: 4,
        PointField.FLOAT64: 8,
    }

    # ------------------------------------------------------------------------------------------------------------------
    #
    #   Public Method
    #
    # ------------------------------------------------------------------------------------------------------------------
    @classmethod
    def point_fields_to_dtypes(
        cls, fields: list[sensor_msgs.PointField], point_step: int
    ) -> tuple[np.dtype[np.void], list[str]]:
        offset = 0
        dtype_list = []
        field_names = []
        for field in fields:
            if offset != field.offset:
                dtype_list += [(f"__EMPTY__{offset + k}", np.uint8) for k in range(field.offset - offset)]
                offset = field.offset

            dtype = cls._point_fields_to_np_dtypes[field.datatype]
            if field.count != 1:
                dtype = np.dtype((dtype, field.count))

            dtype_list.append((field.name, dtype))
            field_names.append(field.name)
            offset += cls._point_field_to_size[field.datatype] * field.count

        if offset != point_step:
            dtype_list += [(f"__EMPTY__{offset + k}", np.uint8) for k in range(point_step - offset)]

        return np.dtype(dtype_list), field_names

    @classmethod
    def dtype_to_fields(
        cls, field_dtype: np.dtype[np.void], dtype_dict: dict[str, tuple[Type[T], int]]
    ) -> tuple[list[sensor_msgs.PointField], int]:
        fields = []
        for name, field in field_dtype.fields.items():  # pyright: ignore [reportOptionalMemberAccess]
            _, offset = field  # pyright: ignore [reportGeneralTypeIssues]
            dtype, count = dtype_dict[name]
            pf = sensor_msgs.PointField()
            pf.name = name
            pf.offset = offset
            pf.datatype = cls._np_dtypes_to_point_fields[dtype]
            pf.count = count
            fields.append(pf)
        return fields, field_dtype.itemsize
