# ------------------------------------------------------------------------------------------------------------------
#
#   Build System
#
# ------------------------------------------------------------------------------------------------------------------
[build-system]
requires = ["setuptools", "wheel"]
build-backend = "setuptools.build_meta"

# ------------------------------------------------------------------------------------------------------------------
#
#   Black
#
# ------------------------------------------------------------------------------------------------------------------
# https://black.readthedocs.io/en/stable/usage_and_configuration/the_basics.html?highlight=line-length#the-basics
[tool.black]
line-length = 120
skip-string-normalization = 1
target-version = ["py38"]

# ------------------------------------------------------------------------------------------------------------------
#
#   Pyright
#
# ------------------------------------------------------------------------------------------------------------------
# https://github.com/microsoft/pyright/blob/main/docs/configuration.md
[tool.pyright]
venv = ".venv"
pythonVersion = 3.8
typeCheckingMode = "strict"

# For undefined stub libraries
reportMissingTypeStubs = "none"
reportUnknownParameterType = "none"
reportUnknownArgumentType = "none"
reportUnknownLambdaType = "none"
reportUnknownVariableType = "none"
reportUnknownMemberType = "none"

# For more strict
reportCallInDefaultInitializer = "error"
reportImplicitStringConcatenation = "error"
reportPropertyTypeMismatch = "error"
reportUninitializedInstanceVariable = "error"
reportUnusedCallResult = "error"
# reportMissingSuperCall = "error"
# reportUnnecessaryTypeIgnoreComment = "error"

# For torch.nn.Module.forward
reportIncompatibleMethodOverride = "none"

# ------------------------------------------------------------------------------------------------------------------
#
#   Isort
#
# ------------------------------------------------------------------------------------------------------------------
# https://pycqa.github.io/isort/docs/configuration/options.html
[tool.isort]
profile = "black"
line_length = 120
skip_gitignore = true
skip_glob = [".git/*"]

# ------------------------------------------------------------------------------------------------------------------
#
#   Bandit
#
# ------------------------------------------------------------------------------------------------------------------
# https://bandit.readthedocs.io/en/latest/config.html
[tool.bandit]
exclude_dirs = [".venv/"]
skips = [
    "B101",
    "B301", # Use Pickle
    "B403", # import Pickle
]

# ------------------------------------------------------------------------------------------------------------------
#
#   pydocstyle
#
# ------------------------------------------------------------------------------------------------------------------
# http://www.pydocstyle.org/en/stable/usage.html
[tool.pydocstyle]
convention = "google"
add-ignore = [
    "D100", # Missing docstring in public module
    "D101", # Missing docstring in public class
    "D102", # Missing docstring in public method
    "D103", # Missing docstring in public function
    "D104", # Missing docstring in public package
    "D105", # Missing docstring in magic method
    "D106", # Missing docstring in public nested class
    "D107", # Missing docstring in __init__
    "D200", # One-line docstring should fit on one line with quotes
    "D212", # Multi-line docstring summary should start at the first line
]

# ------------------------------------------------------------------------------------------------------------------
#
#   docformatter
#
# ------------------------------------------------------------------------------------------------------------------
# https://github.com/PyCQA/docformatter
[tool.docformatter]
make-summary-multi-line = true
pre-summary-newline = true
recursive = true
wrap-summaries = 120
blank = true
