from __future__ import annotations

import genpy
import pytest
from std_msgs.msg import Header


class TestMsg:
    @classmethod
    def compare_msg_equality(cls, source: genpy.Message, target: genpy.Message):
        for key in source.__slots__:
            s = source.__getattribute__(key)
            t = target.__getattribute__(key)
            if isinstance(s, genpy.Message):
                cls.compare_msg_equality(s, t)
            else:
                try:
                    assert s == t
                except AssertionError as e:
                    e.args = (f"AssertionError of \"{key}\" in {source._type} as {e.args[0]}",)
                    raise e

    @classmethod
    def compare_msg_parameters_equality(
        cls, source: genpy.Message, **kwargs  # pyright: ignore [reportMissingParameterType]
    ):
        for key, t in kwargs.items():
            s = source.__getattribute__(key)
            if isinstance(s, genpy.Message):
                cls.compare_msg_equality(s, t)
            else:
                try:
                    assert s == t
                except AssertionError as e:
                    e.args = (f"AssertionError of \"{key}\" in {source._type} as {e.args[0]}",)
                    raise e

    def test_exception(self):
        source = Header(frame_id="a")
        target = Header()

        with pytest.raises(Exception):
            self.compare_msg_equality(source, target)

        with pytest.raises(Exception):
            self.compare_msg_parameters_equality(source, frame_id="b")
