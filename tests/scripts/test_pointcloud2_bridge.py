from __future__ import annotations

import pickle
import time
from pathlib import Path

import numpy as np
import numpy.typing as npt
import pytest
import std_msgs.msg as std_msgs
from rospy import Time
from sensor_msgs.msg import PointCloud2, PointField

from pointcloud2_bridge._pointcloud2_bridge import PointCloud2Bridge

from .msgs.test_msg import TestMsg

FILE_DIR = Path(__file__).parent.absolute()
TEST_SHAPE = (30, 40)
TEST_SHAPE_LIST = [(1, 1), (307200, 1), (1, 307200), (480, 640)]


@pytest.fixture()
def pc2_msg():
    with open(f"{FILE_DIR}/../data/pointcloud2_msg.pkl", "rb") as f:
        d = pickle.load(f)
    return d


@pytest.fixture()
def xyz() -> npt.NDArray[np.float32]:
    return np.random.rand(*TEST_SHAPE, 3).astype(np.float32)


@pytest.fixture()
def rgb() -> npt.NDArray[np.uint8]:
    return np.random.randint(0, 256, (*TEST_SHAPE, 3), np.uint8)


@pytest.fixture()
def rgba() -> npt.NDArray[np.uint8]:
    return np.random.randint(0, 256, (*TEST_SHAPE, 4), np.uint8)


@pytest.fixture()
def label() -> npt.NDArray[np.int8]:
    return np.random.randint(0, 128, (*TEST_SHAPE, 2), np.int8)


@pytest.fixture()
def header():
    float_secs = time.time()
    secs = int(float_secs)
    nsecs = int((float_secs - secs) * 1000000000)

    return std_msgs.Header(
        seq=1,
        stamp=Time(secs, nsecs),
        frame_id="test",
    )


class TestPointCloud2Bridge:
    def test_pointcloud2_to_numpy(self, pc2_msg: PointCloud2):
        ndarray_dict = PointCloud2Bridge().pointcloud2_to_numpy(pc2_msg)

        assert ndarray_dict.keys() == {"xyz", "rgb"}
        assert ndarray_dict["xyz"].shape == ndarray_dict["rgb"].shape == (480, 640, 3)
        assert np.issubdtype(ndarray_dict["xyz"].dtype, np.float32)
        assert np.issubdtype(ndarray_dict["rgb"].dtype, np.uint8)

    def test_pointcloud2_to_numpy_bgr_to_rgb(self, pc2_msg: PointCloud2):
        ndarray_dict1 = PointCloud2Bridge().pointcloud2_to_numpy(pc2_msg, bgr_to_rgb=False)
        ndarray_dict2 = PointCloud2Bridge().pointcloud2_to_numpy(pc2_msg, bgr_to_rgb=True)

        assert np.all(ndarray_dict1["rgb"] == ndarray_dict2["rgb"][..., ::-1]).item()

    @pytest.mark.parametrize("shape", TEST_SHAPE_LIST)
    def test_numpy_to_pointcloud2_multi_shape(self, header: std_msgs.Header, shape: tuple[int, int]):
        xyz = np.random.rand(*shape, 3).astype(np.float32)
        xyz[0, 0, 0] = np.nan

        fields = [
            PointField(name="x", offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name="y", offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name="z", offset=8, datatype=PointField.FLOAT32, count=1),
        ]

        H, W = shape

        msg = PointCloud2Bridge().numpy_to_pointcloud2(xyz=xyz, header=header)
        TestMsg.compare_msg_parameters_equality(
            source=msg,
            header=header,
            height=H,
            width=W,
            fields=fields,
            is_bigendian=False,
            point_step=4 * 3,
            row_step=(4 * 3) * W,
            is_dense=False,
        )

    def test_numpy_to_pointcloud2_force_dense(self, xyz: npt.NDArray[np.float32], header: std_msgs.Header):
        H, W = xyz.shape[:-1]
        fields = [
            PointField(name="x", offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name="y", offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name="z", offset=8, datatype=PointField.FLOAT32, count=1),
        ]
        msg = PointCloud2Bridge().numpy_to_pointcloud2(xyz=xyz, header=header, force_dense=True)
        TestMsg.compare_msg_parameters_equality(
            source=msg,
            header=header,
            height=H,
            width=W,
            fields=fields,
            is_bigendian=False,
            point_step=4 * 3,
            row_step=(4 * 3) * W,
            is_dense=True,
        )

        xyz[0, 0, 0] = np.nan
        xyz[0, 1, 1] = np.inf
        xyz[0, 2, 2] = -np.inf
        msg = PointCloud2Bridge().numpy_to_pointcloud2(xyz=xyz, header=header, force_dense=True)
        TestMsg.compare_msg_parameters_equality(
            source=msg,
            header=header,
            height=1,
            width=H * W - 3,
            fields=fields,
            is_bigendian=False,
            point_step=4 * 3,
            row_step=(4 * 3) * (H * W - 3),
            is_dense=True,
        )

    def test_numpy_to_pointcloud2_rgb(
        self, xyz: npt.NDArray[np.float32], rgb: npt.NDArray[np.uint8], header: std_msgs.Header
    ):
        H, W = xyz.shape[:-1]

        fields = [
            PointField(name="x", offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name="y", offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name="z", offset=8, datatype=PointField.FLOAT32, count=1),
            PointField(name="rgb", offset=12, datatype=PointField.UINT32, count=1),
        ]
        msg = PointCloud2Bridge().numpy_to_pointcloud2(xyz=xyz, rgb=rgb, header=header)
        TestMsg.compare_msg_parameters_equality(
            source=msg,
            header=header,
            height=H,
            width=W,
            fields=fields,
            is_bigendian=False,
            point_step=4 * 4,
            row_step=(4 * 4) * W,
            data=msg.data,  # No check
            is_dense=False,
        )

    def test_numpy_to_pointcloud2_rgba(
        self, xyz: npt.NDArray[np.float32], rgba: npt.NDArray[np.uint8], header: std_msgs.Header
    ):
        fields = [
            PointField(name="x", offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name="y", offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name="z", offset=8, datatype=PointField.FLOAT32, count=1),
            PointField(name="rgba", offset=12, datatype=PointField.UINT32, count=1),
        ]
        msg = PointCloud2Bridge().numpy_to_pointcloud2(xyz=xyz, rgba=rgba, header=header)
        H, W = xyz.shape[:-1]

        TestMsg.compare_msg_parameters_equality(
            source=msg,
            header=header,
            height=H,
            width=W,
            fields=fields,
            is_bigendian=False,
            point_step=4 * 4,
            row_step=(4 * 4) * W,
            is_dense=False,
        )

    def test_numpy_to_pointcloud2_with_other_elements(
        self,
        xyz: npt.NDArray[np.float32],
        rgb: npt.NDArray[np.uint8],
        label: npt.NDArray[np.int8],
        header: std_msgs.Header,
    ):
        H, W = xyz.shape[:-1]

        fields = [
            PointField(name="x", offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name="y", offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name="z", offset=8, datatype=PointField.FLOAT32, count=1),
            PointField(name="rgb", offset=12, datatype=PointField.UINT32, count=1),
            PointField(name="label1", offset=16, datatype=PointField.INT8, count=2),
            PointField(name="label2", offset=18, datatype=PointField.INT8, count=2),
        ]
        msg = PointCloud2Bridge().numpy_to_pointcloud2(
            xyz=xyz, rgb=rgb, others={"label1": label, "label2": label}, header=header
        )
        TestMsg.compare_msg_parameters_equality(
            source=msg,
            header=header,
            height=H,
            width=W,
            fields=fields,
            is_bigendian=False,
            point_step=4 * 4 + 2 * 2,
            row_step=(4 * 4 + 2 * 2) * W,
            is_dense=False,
        )

    def test_bidirectional_msg(self, pc2_msg: PointCloud2):
        ndarray_dict1 = PointCloud2Bridge().pointcloud2_to_numpy(pc2_msg)
        reconstruct_msg1 = PointCloud2Bridge().numpy_to_pointcloud2(
            **ndarray_dict1, header=pc2_msg.header, rgb_to_bgr=False  # pyright: ignore [reportGeneralTypeIssues]
        )
        TestMsg.compare_msg_parameters_equality(
            source=pc2_msg,
            header=pc2_msg.header,
            height=reconstruct_msg1.height,
            width=reconstruct_msg1.width,
            is_bigendian=reconstruct_msg1.is_bigendian,
        )
        ndarray_dict2 = PointCloud2Bridge().pointcloud2_to_numpy(reconstruct_msg1)

        assert ndarray_dict1.keys() == ndarray_dict2.keys()
        for key in ndarray_dict1.keys():
            x1 = np.nan_to_num(ndarray_dict1[key], posinf=0, neginf=0)
            x2 = np.nan_to_num(ndarray_dict2[key], posinf=0, neginf=0)
            assert np.all(x1 == x2).item()

        reconstruct_msg2 = PointCloud2Bridge().numpy_to_pointcloud2(
            **ndarray_dict2, header=pc2_msg.header, rgb_to_bgr=False  # pyright: ignore[reportGeneralTypeIssues]
        )
        TestMsg.compare_msg_equality(reconstruct_msg1, reconstruct_msg2)

    def test_bidirectional_numpy(
        self,
        xyz: npt.NDArray[np.float32],
        header: std_msgs.Header,
    ):
        reconstruct_msg1 = PointCloud2Bridge().numpy_to_pointcloud2(xyz=xyz, header=header)
        ndarray_dict = PointCloud2Bridge().pointcloud2_to_numpy(reconstruct_msg1)
        assert ndarray_dict.keys() == {"xyz"}
        assert np.all(ndarray_dict["xyz"] == xyz).item()

    def test_bidirectional_numpy_rgb(
        self,
        xyz: npt.NDArray[np.float32],
        rgb: npt.NDArray[np.uint8],
        label: npt.NDArray[np.int8],
        header: std_msgs.Header,
    ):
        reconstruct_msg1 = PointCloud2Bridge().numpy_to_pointcloud2(
            xyz=xyz, rgb=rgb, others={"label": label}, header=header, rgb_to_bgr=True
        )
        ndarray_dict = PointCloud2Bridge().pointcloud2_to_numpy(reconstruct_msg1, bgr_to_rgb=True)
        assert ndarray_dict.keys() == {"xyz", "rgb", "label"}
        assert np.all(ndarray_dict["xyz"] == xyz).item()
        assert np.all(ndarray_dict["rgb"] == rgb).item()
        assert np.all(ndarray_dict["label"] == label).item()

        reconstruct_msg1 = PointCloud2Bridge().numpy_to_pointcloud2(
            xyz=xyz, rgb=rgb, others={"label": label}, header=header, rgb_to_bgr=False
        )
        ndarray_dict = PointCloud2Bridge().pointcloud2_to_numpy(reconstruct_msg1, bgr_to_rgb=False)
        assert ndarray_dict.keys() == {"xyz", "rgb", "label"}
        assert np.all(ndarray_dict["xyz"] == xyz).item()
        assert np.all(ndarray_dict["rgb"] == rgb).item()
        assert np.all(ndarray_dict["label"] == label).item()

    def test_bidirectional_numpy_rgba(
        self,
        xyz: npt.NDArray[np.float32],
        rgba: npt.NDArray[np.uint8],
        label: npt.NDArray[np.int8],
        header: std_msgs.Header,
    ):
        reconstruct_msg1 = PointCloud2Bridge().numpy_to_pointcloud2(
            xyz=xyz, rgba=rgba, others={"label": label}, header=header, rgb_to_bgr=True
        )
        ndarray_dict = PointCloud2Bridge().pointcloud2_to_numpy(reconstruct_msg1, bgr_to_rgb=True)
        assert ndarray_dict.keys() == {"xyz", "rgba", "label"}
        assert np.all(ndarray_dict["xyz"] == xyz).item()
        assert np.all(ndarray_dict["rgba"] == rgba).item()
        assert np.all(ndarray_dict["label"] == label).item()

        reconstruct_msg1 = PointCloud2Bridge().numpy_to_pointcloud2(
            xyz=xyz, rgba=rgba, others={"label": label}, header=header, rgb_to_bgr=False
        )
        ndarray_dict = PointCloud2Bridge().pointcloud2_to_numpy(reconstruct_msg1, bgr_to_rgb=False)
        assert ndarray_dict.keys() == {"xyz", "rgba", "label"}
        assert np.all(ndarray_dict["xyz"] == xyz).item()
        assert np.all(ndarray_dict["rgba"] == rgba).item()
        assert np.all(ndarray_dict["label"] == label).item()

    def test_numpy_to_pointcloud2_exception(
        self,
        xyz: npt.NDArray[np.float32],
        rgb: npt.NDArray[np.uint8],
        rgba: npt.NDArray[np.uint8],
        label: npt.NDArray[np.int8],
        header: std_msgs.Header,
    ):
        diff_size_rgb = rgb[:-1]
        diff_size_rgba = rgba[:-1]
        diff_size_label = label[:-1]

        # Simultaneously set RGB and RGBA
        with pytest.raises(Exception):
            PointCloud2Bridge().numpy_to_pointcloud2(xyz=xyz, rgb=rgb, rgba=rgba, header=header)

        # Difference in size between XYZ and RGB
        with pytest.raises(Exception):
            PointCloud2Bridge().numpy_to_pointcloud2(xyz=xyz, rgb=diff_size_rgb, header=header)

        # Difference in size between XYZ and RGBA
        with pytest.raises(Exception):
            PointCloud2Bridge().numpy_to_pointcloud2(xyz=xyz, rgba=diff_size_rgba, header=header)

        # Difference in the number of RGB channels
        with pytest.raises(Exception):
            PointCloud2Bridge().numpy_to_pointcloud2(xyz=xyz, rgb=diff_size_rgba, header=header)

        # Difference in the number of RGBA channels
        with pytest.raises(Exception):
            PointCloud2Bridge().numpy_to_pointcloud2(xyz=xyz, rgba=diff_size_rgb, header=header)

        # Difference in size between XYZ and Label
        with pytest.raises(Exception):
            PointCloud2Bridge().numpy_to_pointcloud2(xyz=xyz, others={"label": diff_size_label}, header=header)
